<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー編集画面</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>


	<body>
		<div class="header">
			<a href="management">ユーザー管理</a>
		</div>
		<div class="main-contents">


			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="errorMessage">
							<li><c:out value="${errorMessage}" />
						</c:forEach>
					</ul>
				</div>
			</c:if>



			<form action="setting" method="post"><br />
				<input name="id" value="${user.id}" type="hidden"/>
				<input name="confirmAccount" value="${user.account}" id="confirmAccount" type="hidden" />

				<label for="account">アカウント</label>
				<input name="account" value="${user.account}" id="account" /> <br /><br>

				<label for="password">パスワード</label>
				<input name="password" type="password" id="password" /> <br /><br>

				<label for="confirmationPassword">パスワード（確認）</label>
  				<input name="confirmationPassword" type="password" id="confirmationPassword" /><br /><br>

  				<label for="name">名前</label>
				<input name="name" value="${user.name}" id="name" /> <br /><br>

				<div class="pulldown">
				<c:if test="${ loginUser.id != user.id }">
				<p>支社
					<select size="1" name="branchId" id="branchId">
						<c:forEach items="${branches}" var="branch">
							<c:if test="${ branch.id == user.branchId }">
								<option value="${branch.id}" selected>${branch.name}</option>
							</c:if>
							<c:if test="${ branch.id != user.branchId }">
								<option value="${branch.id}"> ${branch.name}</option>
							</c:if>
						</c:forEach>
					</select>
				</p>
				</c:if>

				<c:if test="${ loginUser.id == user.id }">
				<p>支社(変更できません)
					<select size="1" name="branchId" id="branchId">
						<c:forEach items="${branches}" var="branch">
							<c:if test="${ branch.id == user.branchId }">
								<option value="${branch.id}" selected>${branch.name}</option>
							</c:if>
						</c:forEach>
					</select>
				</p>
				</c:if>

				<c:if test="${ loginUser.id != user.id }">
				<p>部署
					<select size="1" name="departmentId" id="departmentId">
						<c:forEach items="${departments}" var="department">
							<c:if test="${ department.id == user.departmentId }">
								<option value="${department.id}" selected>${department.name}</option>
							</c:if>
							<c:if test="${ department.id != user.departmentId }">
								<option value="${department.id}"> ${department.name}</option>
							</c:if>
						</c:forEach>
					</select>
				</p>
				</c:if>

				<c:if test="${ loginUser.id == user.id }">
				<p>部署(変更できません)
					<select size="1" name="departmentId" id="departmentId">
						<c:forEach items="${departments}" var="department">
							<c:if test="${ department.id == user.departmentId }">
								<option value="${department.id}" selected>${department.name}</option>
							</c:if>
						</c:forEach>
					</select>
				</p>
				</c:if>
				</div>

				<input type="submit" value="更新" /> <br />
			</form>

			<br><div class="copyright"> Copyright(c)Airi Sasaki</div>

		</div>
</body>
</html>