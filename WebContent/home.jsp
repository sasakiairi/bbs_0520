<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ホーム画面</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>

	<script type="text/javascript">
	function message(){
		if(window.confirm("削除しますか？")){
			document.message.submit();
			return true;
		}else{
			return false;
		}
	}

	function comment(){
		if(window.confirm("削除しますか？")){
			document.comment.submit();
			return true;
		}else{
			return false;
		}
	}
	</script>

	<body>
		<div class="header">
			<div class="menu">
				<a href = "message">新規投稿</a>
				<c:if test = "${loginUser.departmentId == 1 && loginUser.branchId == 1}">
					<a href = "management">ユーザー管理画面</a>
				</c:if>
				<a href = "logout">ログアウト</a>
			</div>
		</div>

		<form action="./" >
			日付<input type="date" value="${start}" name = "start"></input>
			～<input type="date" value="${end}" name = "end"></input><br><br>
			カテゴリ<input name="category" value="${category}" id="category" />
					<a href="#" class="btn-square-shadow">絞込み</a><br>
		</form>

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session"/>
		</c:if>

		<div class="main-contents">
			<div class="messages">
				<c:forEach items="${messages }" var="message">
					<div class="right">
						<div class="">
						<br><div class="name">名前：<c:out value="${message.name}" /></div>
						<div class="title">件名：<c:out value="${message.title}" /></div>
						<div class="category">カテゴリ：<c:out value="${message.category}" /></div>
						<div class="text">投稿内容：<br><c:out value="${message.text}" /></div>
						<div class="date">投稿日時：<c:out value="${message.createdDate }"/></div>
						<c:if test="${ loginUser.id == message.userId }">
							<form action="deleteMessage" method="post">
								<input type="hidden" name="messageId" value="${message.id}">
								<input type="submit" value="投稿削除"onClick="message();return false;">
							</form>
						</c:if>
						</div>
					</div>

					<div class="comments">
						<c:forEach items="${comments}" var="comment">
							<div class="comment">
								<c:if test="${ comment.messageId == message.id }">
									<br><div class="name">名前：<c:out value="${comment.name}" /></div>
									<div class="text">コメント内容：<br><c:out value="${comment.text}" /></div>
									<div class="date">コメント日時：<c:out value="${comment.createdDate }"/></div>
									<c:if test="${ loginUser.id == comment.userId }">
										<form action="deleteComment" method="post">
											<input type="hidden" name="commentId" value="${comment.id}">
											<input type="submit" value="コメント削除" onClick="comment();return false;">
										</form>
									</c:if>
								</c:if>
							</div>
						</c:forEach>
					</div>

					<form action="comment" method="post"><br />
						コメント入力欄（500文字まで)<br>
						<textarea name="text" cols="50" rows="5" class="textlines"></textarea>
						<input type="hidden" name="messageId" value="${message.id}">
						<br><input type="submit" value="コメント投稿"><br/>
					</form>
				</c:forEach>
			</div>
			<br><div class="copyright"> Copyright(c)Airi Sasaki</div>
		</div>
	</body>
</html>