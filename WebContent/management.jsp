<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー管理画面</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<script type="text/javascript">
	function stop(){
		if(window.confirm("アカウント停止させますか？")){
			document.stop.submit();
			return true;
		}
			return false;
	}

	function revival(){
		if(window.confirm("アカウント復活させますか？")){
			document.revival.submit();
			return true;
		}
			return false;
	}

	</script>

	<body>
		<div class="header">
			<c:if test="${ not empty loginUser }">
				<a href="./">ホーム</a>
					<a href="signup">ユーザー新規登録</a>
			</c:if>
		</div>

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session"/>
		</c:if>

		<div class="main-contents">
			<div class="users">
				<c:forEach items="${users}" var="user">
					<div class="user">
						<div class="account">アカウント：<c:out value="${user.account}" /></div><br>
						<div class="name">名前：<c:out value="${user.name}" /></div><br>
						<div class="branchName">支社：<c:out value="${user.branchName}" /></div><br>
						<div class="departmentName">部署：<c:out value="${user.departmentName}" /></div><br>
						<div class="stoppedId"><c:if test="${user.stoppedId == 0 }">アカウント復活停止状態：復活中</c:if></div><br>
						<div class="stoppedId"><c:if test="${user.stoppedId == 1 }">アカウント復活停止状態：停止中</c:if></div>

						<form action="setting" method="get">
							<input type="hidden" name="userId" value="${user.id }">
							<input type="submit" value="編集" />
						</form>
						<c:if test="${ user.id != loginUser.id }"><br />
							<form action="stop" method="post">
								<input type="hidden" name="userId" value="${user.id}">
									<c:if test="${ user.stoppedId == 0 }">
										<input type="hidden" name="stoppedId" value="1">
										<input type="submit" value="停止" onClick="stop();return false;">
									</c:if>
									<c:if test="${ user.stoppedId == 1 }">
										<input type="hidden" name="stoppedId" value="0">
										<input type="submit" value="復活" onClick="revival();return false;">
									</c:if>
							</form><br>
						</c:if>

					</div>
				</c:forEach>
			</div>
			<br><div class="copyright"> Copyright(c)Airi Sasaki</div>
		</div>
</html>