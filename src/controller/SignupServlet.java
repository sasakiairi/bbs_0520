package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignupServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<Branch> branches = new BranchService().select();
		List<Department> departments = new DepartmentService().select();

		request.setAttribute("branches",branches);
		request.setAttribute("departments",departments);
		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {


		List<String> errorMessages = new ArrayList<String>();


		User user = getUser(request);

		if (!isValid(request,errorMessages)) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("user", user);

			List<Branch> branches = new BranchService().select();
			List<Department> departments = new DepartmentService().select();
			request.setAttribute("branches",branches);
			request.setAttribute("departments",departments);

			request.getRequestDispatcher("signup.jsp").forward(request, response);
			return;
		}

		new UserService().insert(user);
		response.sendRedirect("./management");
	}

		private User getUser(HttpServletRequest request) throws IOException, ServletException {
			User user = new User();
			user.setAccount(request.getParameter("account"));
			user.setPassword(request.getParameter("password"));
			user.setName(request.getParameter("name"));
			user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
			user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));

			return user;
		}

		private boolean isValid(HttpServletRequest request,List<String> errorMessages) {

			String account = request.getParameter("account");
			String password = request.getParameter("password");
			String name = request.getParameter("name");
			int branchId = Integer.parseInt(request.getParameter("branchId"));
			int departmentId = Integer.parseInt(request.getParameter("departmentId"));
			String confirmationPassword = request.getParameter("confirmationPassword");

			User confirmUser = new UserService().select(account);
;

			if (StringUtils.isBlank(account)) {
				errorMessages.add("アカウントを入力してください");
			}else if(!account.matches("^[0-9a-zA-Z]+$")) {
				errorMessages.add("アカウントは半角英数字で入力してください");
			}else if (20 < account.length()) {
				errorMessages.add("アカウント名は6文字以上20文字以下で入力してください");
			}else if (6 > account.length()) {
				errorMessages.add("アカウント名は6文字以上20文字以下で入力してください");
			}

			if (confirmUser != null) {
				errorMessages.add("アカウントが重複しているため登録できません");
			}


			if (StringUtils.isBlank(password) && (StringUtils.isBlank(confirmationPassword))) {
				errorMessages.add("パスワードを入力してください");
			}else if(!password.equals(confirmationPassword)) {
				errorMessages.add("入力したパスワードと確認用パスワードが一致しません");
			}else if (20 < password.length()) {
				errorMessages.add("パスワードは6文字以上20文字以下で入力してください");
			}else if(6 > password.length()) {
				errorMessages.add("パスワードは6文字以上20文字以下で入力してください");
			}

			if (StringUtils.isBlank(name)) {
				errorMessages.add("名前を入力してください");
			}else if (10 < name.length()) {
				errorMessages.add("名前は10文字以下で入力してください");
			}


			if ((branchId == 1 && (departmentId >= 3) ) || (branchId >= 2 && departmentId <= 2)) {
				errorMessages.add("支社と部署の組み合わせが不正です");
			}

			if (errorMessages.size() >= 1) {
				return false;
			}
			return true;
		}

}