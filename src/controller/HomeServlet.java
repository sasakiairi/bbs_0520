package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserComment;
import beans.UserMessage;
import service.CommentService;
import service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		request.setCharacterEncoding("UTF-8");

		String userId = request.getParameter("user_id");
		String startDate = request.getParameter("start");
		String endDate = request.getParameter("end");
		String categoryNarrow = request.getParameter("category");
		List<UserMessage> messages = new MessageService().select(userId, startDate, endDate,categoryNarrow);
		List<UserComment> comments = new CommentService().select(userId);


		request.setAttribute("start", startDate);
		request.setAttribute("end", endDate);
		request.setAttribute("category", categoryNarrow);
		request.setAttribute("messages",messages);
		request.setAttribute("comments", comments);

		request.getRequestDispatcher("/home.jsp").forward(request, response);

	}

}