package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import beans.UserBranchDepartment;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("loginUser");

		List<Branch> branches = new BranchService().select();
		List<Department> departments = new DepartmentService().select();
		List<String> errorMessages = new ArrayList<String>();

		String id = request.getParameter("userId");

		if (id.matches("^[0-9]+$")) {
			User user = new UserService().select(Integer.parseInt(request.getParameter("userId")));
			if (user == null) {
				errorMessages.add("不正なパラメータが入力されました");
				request.setAttribute("errorMessages", errorMessages);
				List<UserBranchDepartment> users = new UserService().select();

				request.setAttribute("users", users);
				request.setAttribute("loginUser", loginUser);

				request.getRequestDispatcher("management.jsp").forward(request, response);
				return;
			} else {
				request.setAttribute("user", user);
			}
		} else {
			errorMessages.add("不正なパラメータが入力されました");
			request.setAttribute("errorMessages", errorMessages);
			List<UserBranchDepartment> users = new UserService().select();

			request.setAttribute("users", users);
			request.setAttribute("loginUser", loginUser);

			request.getRequestDispatcher("management.jsp").forward(request, response);
			return;
		}

		request.setAttribute("loginUser" ,loginUser);

		request.setAttribute("branches",branches);
		request.setAttribute("departments",departments);
		request.getRequestDispatcher("setting.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<String> errorMessages = new ArrayList<String>();
		User user = getUser(request);
		String confirmationPassword = request.getParameter("confirmationPassword");

		if(!isValid(user,request,confirmationPassword,errorMessages)) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("user", user);

			List<Branch> branches = new BranchService().select();
			List<Department> departments = new DepartmentService().select();
			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);

			request.getRequestDispatcher("/setting.jsp").forward(request, response);
			return;
		}

		new UserService().update(user,confirmationPassword);
		response.sendRedirect("./management");
 	}

	private User getUser(HttpServletRequest request) throws IOException, ServletException {

		User user = new User();
		user.setId(Integer.parseInt(request.getParameter("id")));
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setName(request.getParameter("name"));
		user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));
		user.setStoppedId(0);
		return user;
	}


	private boolean isValid(User user,HttpServletRequest request,String confirmationPassword, List<String> errorMessages) {

		String name = user.getName();
		String account = user.getAccount();
		String password = user.getPassword();
		int branchId = user.getBranchId();
		int departmentId = user.getDepartmentId();
		User confirmUser = new UserService().select(account);
		String confirmAccount = request.getParameter("confirmAccount");

		if (StringUtils.isBlank(account)) {
			errorMessages.add("アカウント名を入力してください");
		}else if (!account.matches( "[0-9a-zA-Z]+$")) {
			errorMessages.add("アカウント名は半角英数字で入力してください");
		} else if (20 < account.length()) {
			errorMessages.add("アカウント名は6文字以上20文字以下で入力してください");
		} else if (6 > account.length()) {
			errorMessages.add("アカウント名は6文字以上20文字以下で入力してください");
		}

		if ((confirmUser != null) && (!confirmAccount.equals(account) )) {
			errorMessages.add("アカウントが重複しているため登録できません");
		}

		if (!StringUtils.isBlank(password)) {
			if (20 < password.length()) {
				errorMessages.add("パスワードは6文字以上20文字以下で入力してください");
			} else if (6 > password.length()) {
				errorMessages.add("パスワードは6文字以上20文字以下で入力してください");
			}
		}

		if (!password.equals(confirmationPassword)) {
			errorMessages.add("入力したパスワードと確認用パスワードが一致しません");
		}

		if (!StringUtils.isBlank(name) && (10 < name.length())) {
			errorMessages.add("名前は10文字以下で入力してください");
		}
		if(StringUtils.isBlank(name)) {
			errorMessages.add("名前を入力してください");
		}

		if ((branchId == 1 && (departmentId >= 3) ) || (branchId >= 2 && departmentId <= 2)) {
			errorMessages.add("支社と部署の組み合わせが不正です");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}