package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		request.getRequestDispatcher("login.jsp").forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		String account = request.getParameter("account");
		String password = request.getParameter("password");
		User user = new UserService().select(account, password);
		List<String> errorMessages = new ArrayList<String>();

		if(!isValid(user,errorMessages,account,password)) {
			request.setAttribute("account", account);
			request.setAttribute("password", password);
			request.setAttribute("errorMessages", errorMessages );
			request.getRequestDispatcher("login.jsp").forward(request,response);
			return;
		}

		HttpSession session = request.getSession();
		session.setAttribute("loginUser", user);
		response.sendRedirect("./");
	}

	private boolean isValid(User user,List<String> errorMessages,String account,String password){
		if(StringUtils.isBlank(account)) {
			errorMessages.add("アカウント名を入力してください");
		}else if(!account.matches("^[0-9a-zA-Z]+$")) {
			errorMessages.add("アカウントは半角英数字で入力してください");
		}

		if(StringUtils.isBlank(password)) {
			errorMessages.add("パスワードを入力してください");
		}

		if(!(StringUtils.isBlank(account) || StringUtils.isBlank(password))) {
			if(user == null) {
				errorMessages.add("アカウント名、またはパスワードが誤りです");
			}else if(user.getStoppedId() == 1) {
				errorMessages.add("アカウント名、またはパスワードが誤りです");
			}
		}

		if(errorMessages.size() >=1) {
			return false;
		}
		return true;
	}


}