package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter({"/signup","/setting/*","/management"})
public class ManagementFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpSession session = req.getSession();
		User user = (User) session.getAttribute("loginUser") ;
		int departmentId = user.getDepartmentId();
		int branchId = user.getBranchId();
		List<String> errorMessages = new ArrayList<String>();

		if (!(branchId == 1 && departmentId == 1)) {

			errorMessages.add("アクセス権限がありません");
			request.setAttribute("errorMessages", errorMessages);

			request.getRequestDispatcher("./").forward(request, response);
			return;
		}


		chain.doFilter(request, response); // サーブレットを実行

	}

	@Override
	public void init(FilterConfig config) {
	}

	@Override
	public void destroy() {
	}


}
