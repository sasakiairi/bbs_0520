package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import com.mysql.jdbc.StringUtils;

import beans.Message;
import beans.UserMessage;
import dao.MessageDao;
import dao.UserMessageDao;

public class MessageService {

	public void insert(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<UserMessage> select(String userId, String startDate, String endDate, String categoryNarrow) {
		final int LIMIT_NUM = 1000;

		Connection connection = null;
		try {
			connection = getConnection();
			Integer id = null;
			if(!StringUtils.isNullOrEmpty(userId)) {
				id = Integer.parseInt(userId);
			}

			String start = "2020-01-01 00:00:00 ";
			if(!StringUtils.isNullOrEmpty(startDate)) {
				start = startDate + " 00:00:00 ";
			}

			String end = "CURRENT_TIMESTAMP ";
			if(!StringUtils.isNullOrEmpty(endDate)) {
				end = endDate + " 23:59:59 ";
			}

			String category = null;
			if(!StringUtils.isNullOrEmpty(categoryNarrow)) {
				category = "%" + categoryNarrow + "%";
			}

			List<UserMessage> messages = new UserMessageDao().select(connection,id,start,end,category,LIMIT_NUM);
			commit(connection);

			return messages;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void delete(int deleteMessageId) {
		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().delete(connection, deleteMessageId);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}
}